package stup_klient;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {
    public ServerSocket socket;

    private boolean running = false;
    private Board enemyBoard, playerBoard;

    private int shipsToPlace = 5;

    private boolean enemyTurn = false;

    private Random random = new Random();

    private Label statusLabel = new Label("");

    private Parent createContent() throws Exception {
        BorderPane root = new BorderPane();
        root.setPrefSize(800, 800);

        HBox hb = new HBox();
        VBox vb = new VBox();
        Label label1 = new Label("Enemy id:");
        Label label2 = new Label("");
        TextField textField = new TextField();
        Button submit = new Button("Submit");
        hb.getChildren().addAll(
                label1, textField, submit
        );
        vb.getChildren().addAll(
                new Text(String.format("Client id: %d", socket.clientId)),
                hb,
                label2
        );
        hb.setSpacing(10);
        vb.setSpacing(10);

        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if ((textField.getText() != null && !textField.getText().isEmpty())) {
                    try {
                        label2.setText("Connecting...");
                        boolean hasEnemy = socket.sendEnemyId(textField.getText());
                        if (hasEnemy) {
                            label2.setText("Enemy id: " + textField.getText());
                            if (Integer.parseInt(textField.getText()) > socket.clientId) {
                                enemyTurn = true;
                            }
                            submit.setOnAction(null);
                        } else {
                            label2.setText("Enemy with this id does not exist");
                        }
                    } catch (Exception exc) {
                        return;
                    }
                } else {
                    label2.setText("You have not input enemy id.");
                }
            }
        });

        root.setLeft(vb);
        root.setRight(statusLabel);

        enemyBoard = new Board(true, event -> {
            if (!running)
                return;

            Cell cell = (Cell) event.getSource();
            socket.sendShoot(cell);
            if (cell.wasShot)
                return;

            enemyTurn = !cell.shoot();

            if (enemyBoard.ships == 0) {
                System.out.println("YOU WIN");
                System.exit(0);
            }

            if (enemyTurn) {
                try {
                    enemyMove();
                } catch (Exception e) {
                    return;
                }
            }
        });

        playerBoard = new Board(false, event -> {
            if (running)
                return;

            Cell cell = (Cell) event.getSource();
            if (playerBoard.placeShip(new Ship(shipsToPlace, event.getButton() == MouseButton.PRIMARY), cell.x, cell.y)) {
                if (--shipsToPlace == 0) {
                    socket.sendBoard(playerBoard.getBoard());
                    try {
                        startGame();
                    } catch (Exception e) {
                        return;
                    }
                }
            }
        });

        VBox vbox = new VBox(50, enemyBoard, playerBoard);
        vbox.setAlignment(Pos.CENTER);

        root.setCenter(vbox);

        return root;
    }

    private void enemyMove() throws Exception {
        while (enemyTurn) {
            String[] coords = socket.getEnemyShoot();

            Cell cell = playerBoard.getCell(
                    Integer.parseInt(coords[0]),
                    Integer.parseInt(coords[1])
            );
            if (cell.wasShot)
                continue;

            enemyTurn = cell.shoot();
            setTurnLabel();

            if (playerBoard.ships == 0) {
                System.out.println("YOU LOSE");
                System.exit(0);
            }
        }
    }

    private void startGame() throws Exception {
        // place enemy ships
        statusLabel.setText("Waiting for enemy board");
        String enemyBd = socket.getBoard();
        statusLabel.setText("READY");
        String[] ships = enemyBd.substring(2).split(";");

        for (String s : ships) {
            String[] shipParams = s.split(",");
            int type = Integer.parseInt(shipParams[0]);
            boolean vertical = Boolean.parseBoolean(shipParams[1]);
            int x = Integer.parseInt(shipParams[2]);
            int y = Integer.parseInt(shipParams[3]);
            enemyBoard.placeShip(new Ship(type, vertical), x, y);
        }

        setTurnLabel();
        running = true;

        if (enemyTurn) {
            try {
                enemyMove();
            } catch (Exception e) {
                return;
            }
        }
    }

    private void setTurnLabel() {
        if (enemyTurn) {
            statusLabel.setText("Enemy turn");
        } else {
            statusLabel.setText("Your turn");
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parameters parameters = getParameters();
        socket = new ServerSocket(parameters);
        Scene scene = new Scene(createContent());
        primaryStage.setTitle("Battleship");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}