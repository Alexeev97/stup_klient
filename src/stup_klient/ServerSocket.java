package stup_klient;

import javafx.application.Application;

import java.io.*;
import java.net.Socket;

public class ServerSocket {
    public Socket clientSocket;
    public PrintWriter outToServer;
    public BufferedReader inFromServer;
    public int clientId;
    public int enemyId;

    public ServerSocket(Application.Parameters params) throws Exception {
        String host = params.getNamed().get("host");
        String port = params.getNamed().get("port");
        System.out.println(host + " " + port);
        clientSocket = new Socket(host != null ? host : "localhost", port != null ? Integer.parseInt(port) : 6969);
        outToServer = new PrintWriter(clientSocket.getOutputStream(), true);
        inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        outToServer.println("RDY");
        String line = inFromServer.readLine();
        if (line != null) {
            clientId = Integer.parseInt(line);
        }
    }

    public boolean sendEnemyId(String enemyId) throws Exception {
        while (true) {
            outToServer.println(enemyId);
            String line = inFromServer.readLine();
            if (line.startsWith("EI")) {
                this.enemyId = Integer.parseInt(enemyId);
                break;
            }
        }

        return true;
    }

    public boolean sendBoard(String board) {
        outToServer.println("BO" + board);
        return true;
    }

    public String getBoard() throws Exception {
        while (true) {
            String line = inFromServer.readLine();
            if (line.startsWith("BO")) {
                return line;
            }
        }
    }

    public String[] getEnemyShoot() throws Exception {
        while (true) {
            String line = inFromServer.readLine();
            if (line.startsWith("ES")) {
                return line.substring(2).split(",");
            }
        }
    }

    public void sendShoot(Cell cell) {
        outToServer.println(String.format("ES%d,%d", cell.x, cell.y));
    }
}
