# stup_klient

Klient projektu na SK2 - Aleksander Stupnicki

uruchomienie z jara:

`java -jar '.\out\artifacts\gui_jar\gui.jar'`

lub

`java -jar ./out/artifacts/gui_jar/gui.jar`

opcja z innym hostem:

`java -jar '.\out\artifacts\gui_jar\gui.jar' --host=192.168.10.1`

opcja z innym portem:

`java -jar '.\out\artifacts\gui_jar\gui.jar' --port=8282`

obie naraz:

`java -jar '.\out\artifacts\gui_jar\gui.jar' --host=192.168.10.1 --port=8282`